import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

describe("Bonus system test", () => {
  console.log("Tests started");

  test("Standard", () => {
    let program = "Standard";
    expect(calculateBonuses(program, 100)).toBe(0.05);
    expect(calculateBonuses(program, 10000)).toBe(0.05 * 1.5);
    expect(calculateBonuses(program, 50000)).toBe(0.05 * 2);
    expect(calculateBonuses(program, 100000)).toBe(0.05 * 2.5);
  });

  test("Premium", () => {
    const program = "Premium";
    expect(calculateBonuses(program, 100)).toBe(0.1);
    expect(calculateBonuses(program, 10000)).toBe(0.1 * 1.5);
    expect(calculateBonuses(program, 50000)).toBe(0.1 * 2);
    expect(calculateBonuses(program, 100000)).toBe(0.1 * 2.5);
  });

  test("Diamond", () => {
    const program = "Diamond";
    expect(calculateBonuses(program, 100)).toBe(0.2);
    expect(calculateBonuses(program, 10000)).toBe(0.2 * 1.5);
    expect(calculateBonuses(program, 50000)).toBe(0.2 * 2);
    expect(calculateBonuses(program, 100000)).toBe(0.2 * 2.5);
  });

  test("IncorrectData", () => {
    const program = "IncorrectData";
    expect(calculateBonuses(program, 100)).toBe(0);
    expect(calculateBonuses(program, 10000)).toBe(0);
    expect(calculateBonuses(program, 50000)).toBe(0);
    expect(calculateBonuses(program, 100000)).toBe(0);
  });

  console.log("Tests Finished");
});
